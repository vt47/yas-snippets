;;; Compiled snippets and support files for `misc'
;;; Snippet definitions:
;;;
(yas-define-snippets 'misc
                     '(("lorem" "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc rutrum nisl ut ante dignissim, at tempor elit pellentesque. Donec cursus semper arcu semper vestibulum. Morbi et nibh quis nulla mollis vehicula nec luctus nulla. In eleifend rhoncus sagittis. Donec non odio vel neque laoreet aliquet vel quis dui. Duis id metus nisl. Aliquam a est in neque maximus maximus. Maecenas fringilla nibh porta libero eleifend, quis tempus leo auctor. Vestibulum non felis vel nibh laoreet semper vel eu nisi.$1" "lorem ipsum" nil nil nil "/Users/vutran/.emacs.d/snippets/misc/lorem-ipsum" nil nil)
                       ("lurl" "http://localhost:${1: 3000}\n" "localhost" nil nil nil "/Users/vutran/.emacs.d/snippets/misc/localhostport" nil nil)))


;;; Do not edit! File generated at Mon Jan  8 17:28:49 2018
